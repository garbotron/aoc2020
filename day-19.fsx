type RuleSet = int list
type Rule = CharRule of char | RefRule of RuleSet list

let parseRule (line: string) =
  match line.Split ':' with
  | [| numStr; bodyStr |] ->
    let num = int numStr
    let body = bodyStr.Trim()
    if body.StartsWith "\"" then Some (num, CharRule body.[1])
    else
      let parseRuleSet (str: string) = str.Split ' ' |> List.ofArray |> List.map int
      Some (num, RefRule (body.Split " | " |> List.ofArray |> List.map parseRuleSet))
  | _ -> None

let input = System.IO.File.ReadAllLines("day-19-input.txt")
let messages = input.[Array.findIndex ((=) "") input + 1..]

// Returns a sequence of strings, one per successful parsing (the string is the unconsumed part of the input).
let rec ruleMatches (rules: Map<int, Rule>) (msg: string) (ruleIdx: int): string list =
  match rules.[ruleIdx] with
  | CharRule c -> if msg.Length > 0 && msg.[0] = c then [msg.[1..]] else []
  | RefRule sets -> sets |> List.collect (ruleSetMatches rules msg)

and ruleSetMatches rules msg set =
  match set with
  | [] -> []
  | [r] -> ruleMatches rules msg r
  | r :: tail -> ruleMatches rules msg r |> List.collect (fun x -> ruleSetMatches rules x tail)

let isFullMatch rules ruleIdx msg =
  ruleMatches rules msg ruleIdx |> Seq.exists ((=) "")

// Part 1
let rulesP1 = input |> Seq.choose parseRule |> Map
messages |> Seq.filter (isFullMatch rulesP1 0) |> Seq.length |> printfn "%d"

// Part 2 (input is amended)
let rulesP2 = rulesP1 |> Map.add 8 (RefRule [[42]; [42; 8]]) |> Map.add 11 (RefRule [[42; 31]; [42; 11; 31]])
messages |> Seq.filter (isFullMatch rulesP2 0) |> Seq.length |> printfn "%d"
