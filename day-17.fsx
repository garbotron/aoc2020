type State = Set<int * int * int * int> // in set = active, not in set = inactive

let parseInput (lines: string array): State =
  lines
  |> Array.map (Seq.indexed >> Seq.choose (fun (i, c) -> if c = '#' then Some i else None))
  |> Array.indexed
  |> Seq.collect (fun (y, xs) -> xs |> Seq.map (fun x -> (x, y)))
  |> Seq.map (fun (x, y) -> (x, y, 0, 0))
  |> Set.ofSeq

let neighbors3D (x, y, z, _) = seq {
  for i in { x - 1 .. x + 1 } do
    for j in { y - 1 .. y + 1 } do
      for k in { z - 1 .. z + 1 } -> i, j, k, 0 } |> Seq.except [(x, y, z, 0)]

let neighbors4D (x, y, z, w) = seq {
  for i in { x - 1 .. x + 1 } do
    for j in { y - 1 .. y + 1 } do
      for k in { z - 1 .. z + 1 } do
        for l in { w - 1 .. w + 1 } -> i, j, k, l } |> Seq.except [(x, y, z, w)]

let cubesToConsider neighbors state = state |> Set.toSeq |> Seq.collect neighbors |> Seq.distinct

let nextState neighbors state: State =
  let activeNeighbors coords = neighbors coords |> Seq.filter (fun x -> Set.contains x state) |> Seq.length
  let isActive coords =
    match activeNeighbors coords with
    | 2 when Set.contains coords state -> true
    | 3 -> true
    | _ -> false
  state |> cubesToConsider neighbors |> Seq.filter isActive |> Set.ofSeq

let input =
  System.IO.File.ReadAllLines("day-17-input.txt")
  |> parseInput

// Part 1
{ 0 .. 5 }
  |> Seq.fold (fun s _ -> nextState neighbors3D s) input
  |> Set.toSeq
  |> Seq.length
  |> printfn "%d"

// Part 2
{ 0 .. 5 }
  |> Seq.fold (fun s _ -> nextState neighbors4D s) input
  |> Set.toSeq
  |> Seq.length
  |> printfn "%d"
