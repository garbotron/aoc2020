System.IO.File.ReadAllText("day-06-input.txt")
  |> fun x -> x.Split "\n\n" // isolate the groups
  |> Seq.map (Seq.distinct >> Seq.filter System.Char.IsLetter) // find all unique letters
  |> Seq.sumBy Seq.length // sum up all the sequence lengths
  |> printfn "%d"
