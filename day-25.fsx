let input = System.IO.File.ReadAllLines "day-25-input.txt"
let pk1 = int64 input.[0]
let pk2 = int64 input.[1]

let transforms sn = Seq.initInfinite id |> Seq.scan (fun x _ -> (x * sn) % 20201227L) 1L
let findLoopSize pk = transforms 7L |> Seq.findIndex ((=) pk)

let ls2 = findLoopSize pk2
let key = transforms pk1 |> Seq.skip ls2 |> Seq.head

printfn "%d" key
