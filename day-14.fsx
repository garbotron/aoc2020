open System.Text.RegularExpressions

type Bit = One | Zero | DontCare
type Instruction = Set of addr: int64 * value: int64 | Mask of Bit list
type SystemState = {
  Mask: Bit list
  Memory: Map<int64, int64>
}

let parseInstruction str =
  let maskMatch = Regex.Match (str, "mask = ([01X]+)")
  if maskMatch.Success then
    let parseBit ch = match ch with | '0' -> Zero | '1' -> One | _ -> DontCare
    maskMatch.Groups.[1].Value |> Seq.map parseBit |> Seq.rev |> Seq.toList |> Mask
  else
    let setMatch = Regex.Match (str, @"mem\[([0-9]+)] = ([0-9]+)")
    if setMatch.Success then Set (addr = int64 setMatch.Groups.[1].Value, value = int64 setMatch.Groups.[2].Value)
    else raise <| System.Exception()

let maskedValue (value: int64) (mask: Bit list): int64 =
  let bitAddend idx bit =
    match bit with
    | Zero -> 0L
    | One -> 1L <<< idx
    | DontCare -> (value &&& (1L <<< idx))
  mask |> Seq.indexed |> Seq.fold (fun v (i, b) -> v + (bitAddend i b)) 0L

let rec allMaskedValues (value: int64) (mask: Bit list): int64 seq =
  let addAndShift a v = (v <<< 1) + a
  match mask with
  | One :: tail -> Seq.map (addAndShift 1L) (allMaskedValues (value >>> 1) tail)
  | Zero :: tail -> Seq.map (addAndShift (value &&& 1L)) (allMaskedValues (value >>> 1) tail)
  | DontCare :: tail -> seq {
      yield! Seq.map (addAndShift 0L) (allMaskedValues (value >>> 1) tail)
      yield! Seq.map (addAndShift 1L) (allMaskedValues (value >>> 1) tail) }
  | [] -> seq { 0L }

let exec1 (state: SystemState) (instr: Instruction): SystemState =
  match instr with
  | Mask m -> { state with Mask = m }
  | Set (addr, value) -> { state with Memory = state.Memory |> Map.add addr (maskedValue value state.Mask) }

let exec2 (state: SystemState) (instr: Instruction): SystemState =
  match instr with
  | Mask m -> { state with Mask = m }
  | Set (addr, value) ->
    let folder mem addr = Map.add addr value mem
    { state with Memory = allMaskedValues addr state.Mask |> Seq.fold folder state.Memory }

let input = System.IO.File.ReadAllLines("day-14-input.txt") |> Seq.map parseInstruction |> Seq.toArray

// Part 1
input
  |> Seq.fold exec1 { Mask = List.replicate 36 DontCare; Memory = Map.empty }
  |> fun x -> Map.toList x.Memory
  |> Seq.sumBy (fun (i, v) -> v)
  |> printfn "%d"

// Part 2
input
  |> Seq.fold exec2 { Mask = List.replicate 36 DontCare; Memory = Map.empty }
  |> fun x -> Map.toList x.Memory
  |> Seq.sumBy (fun (i, v) -> v)
  |> printfn "%d"
