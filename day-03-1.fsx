let inputLines = System.IO.File.ReadLines("day-03-input.txt") |> Array.ofSeq

let xStep = 3
let yStep = 1

let isOnMap y = y < inputLines.Length && inputLines.[y] <> ""
let isTree x y =
  let line = inputLines.[y]
  let ch = line.[x % line.Length]
  ch = '#'

let rec hop x y treeCount =
  if isOnMap y then
    let tree = if isTree x y then 1 else 0
    hop (x + xStep) (y + yStep) (treeCount + tree)
  else treeCount

printfn "%d" (hop 0 0 0)
