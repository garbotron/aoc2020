type Token = Plus | Star | OpenParen | CloseParen | Number of int64

let parseLine (str: string) =
  let parseToken ch =
    match ch with
    | '+' -> Some Plus
    | '*' -> Some Star
    | '(' -> Some OpenParen
    | ')' -> Some CloseParen
    | x when x >= '0' && x <= '9' -> Some (Number (x |> string |> int64))
    | _ -> None
  str |> Seq.map parseToken |> Seq.choose id |> List.ofSeq

let rec calcP1 input =
  let resolveNumber = function
    | Number x :: rest -> x, rest
    | OpenParen :: rest -> calcP1 rest
    | _ -> raise <| System.Exception()
  let lhs, input = resolveNumber input
  let oper, input = List.head input, List.tail input
  let rhs, input = resolveNumber input
  let result =
    match oper with
    | Plus -> lhs + rhs
    | Star -> lhs * rhs
    | _ -> raise <| System.Exception()
  match input with
    | CloseParen :: rest -> result, rest
    | [] -> result, []
    | x -> calcP1 (Number result :: x)

let rec calcP2 input =
  match input |> flattenParens |> flattenArith Plus (+) |> flattenArith Star (*) with
  | [Number x] -> x
  | _ -> raise <| System.Exception()

and flattenParens input =
  match List.tryFindIndexBack ((=) OpenParen) input with
  | None -> input // no parens found
  | Some i ->
    let j = i + List.findIndex ((=) CloseParen) input.[i..]
    flattenParens (input.[..i-1] @ [Number (calcP2 input.[i+1..j-1])] @ input.[j+1..])

and flattenArith tok func input =
  match List.tryFindIndex ((=) tok) input with
  | None -> input // no operator token found
  | Some i ->
    match input.[i-1], input.[i+1] with
    | Number a, Number b -> flattenArith tok func (input.[..i-2] @ [Number (func a b)] @ input.[i+2..])
    | _ -> raise <| System.Exception()

// Part 1
System.IO.File.ReadAllLines("day-18-input.txt")
  |> Seq.sumBy (parseLine >> calcP1 >> fst)
  |> printfn "%d"

// Part 2
System.IO.File.ReadAllLines("day-18-input.txt")
  |> Seq.sumBy (parseLine >> calcP2)
  |> printfn "%d"
