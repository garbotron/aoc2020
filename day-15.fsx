open System.Collections.Generic

let input = [0; 13; 1; 8; 6; 15]

type State = {
  TurnResults: int array
  LastTimeSpoken: Dictionary<int, int>
  TimeBeforeThat: Dictionary<int, int>
}

let build (state: State) (n: int) =
  let mutable x = 0
  let result =
    if n < input.Length then input.[n]
    elif state.TimeBeforeThat.TryGetValue(state.TurnResults.[n - 1], &x) then (n - 1) - x
    else 0
  state.TurnResults.[n] <- result
  if state.LastTimeSpoken.TryGetValue(result, &x) then
    state.TimeBeforeThat.[result] <- x
  state.LastTimeSpoken.[result] <- n

let solve n =
  let state = { TurnResults = Array.create n 0; LastTimeSpoken = Dictionary(); TimeBeforeThat = Dictionary() }
  { 0 .. n - 1 } |> Seq.iter (build state)
  state.TurnResults.[n - 1]

// Part 1
solve 2020 |> printfn "%d"

// Part 2
solve 30000000 |> printfn "%d"
