open System.Text.RegularExpressions

let isValidHeight str =
  let cm = Regex.Match(str, "^([0-9]+)cm$")
  if cm.Success then
    let x = int cm.Groups.[1].Value
    x >= 150 && x <= 193
  else
    let inches = Regex.Match(str, "^([0-9]+)in$")
    if inches.Success then
      let x = int inches.Groups.[1].Value
      x >= 59 && x <= 76
    else
      false

let validateField pairs fieldName predicate =
  let pair = pairs |> Seq.tryFind (fun (name, _) -> name = fieldName)
  match pair with
  | None -> false
  | Some (_, value) -> predicate value

let isValidPassport pairs =
  let validations = [
    validateField pairs "byr" (fun x -> int x >= 1920 && int x <= 2002)
    validateField pairs "iyr" (fun x -> int x >= 2010 && int x <= 2020)
    validateField pairs "eyr" (fun x -> int x >= 2020 && int x <= 2030)
    validateField pairs "hgt" isValidHeight
    validateField pairs "hcl" (fun x -> Regex.IsMatch(x, "^#[0-9a-f]{6}$"))
    validateField pairs "ecl" (fun x -> List.contains x ["amb"; "blu"; "brn"; "gry"; "grn"; "hzl"; "oth"])
    validateField pairs "pid" (fun x -> Regex.IsMatch(x, "^[0-9]{9}$"))
  ]
  List.forall ((=) true) validations

let parseRecord str =
  Regex.Matches(str, @"([\S+]+):([\S+]+)") |> Seq.map (fun x -> x.Groups.[1].Value, x.Groups.[2].Value)

System.IO.File.ReadAllText("day-04-input.txt")
  |> fun x -> x.Split "\n\n" // isolate the password records
  |> Seq.map parseRecord // break them into name:value pairs
  |> Seq.filter isValidPassport
  |> Seq.length
  |> printfn "%d"
