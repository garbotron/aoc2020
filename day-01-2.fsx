let numbers = System.IO.File.ReadLines("day-01-input.txt")
              |> Seq.filter (not << System.String.IsNullOrWhiteSpace)
              |> Seq.map System.Int32.Parse
              |> Array.ofSeq

for i in 0 .. numbers.Length - 3 do
  for j in i + 1 .. numbers.Length - 2 do
    for k in j + 1 .. numbers.Length - 1 do
      if numbers.[i] + numbers.[j] + numbers.[k] = 2020 then
        printfn "%d" (numbers.[i] * numbers.[j] * numbers.[k])
