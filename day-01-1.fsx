let numbers = System.IO.File.ReadLines("day-01-input.txt")
              |> Seq.filter (not << System.String.IsNullOrWhiteSpace)
              |> Seq.map System.Int32.Parse

// Track whether or not each number 0..2019 has been seen yet.
let numbersSeen = Array.create 2020 false

// When we encounter a number x, if we have seen (2020 - x), we have a match.
for number in numbers do
  Array.set numbersSeen number true
  if numbersSeen.[2020 - number] then
    let product = (2020 - number) * number
    printfn "%d" product
