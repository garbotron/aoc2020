let decodeNumber (zero: char) (one: char) (str: string) =
  let binaryStr = str.Replace(zero, '0').Replace(one, '1')
  System.Convert.ToInt32(binaryStr, 2)

let getSeatId (boardingPass: string) =
  let row = decodeNumber 'F' 'B' boardingPass.[0 .. 6]
  let col = decodeNumber 'L' 'R' boardingPass.[7 .. 9]
  (row * 8) + col

System.IO.File.ReadAllLines("day-05-input.txt")
  |> Seq.map getSeatId
  |> Seq.max
  |> printfn "%d"
