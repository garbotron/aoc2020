open System.Text.RegularExpressions

let hasRequiredField (pairs: MatchCollection) (name: string) =
  pairs |> Seq.exists (fun x -> x.Groups.[1].Value = name)

let isValidPassport (pairs: MatchCollection) =
  let reqFields = ["byr"; "iyr"; "eyr"; "hgt"; "hcl"; "ecl"; "pid"]
  reqFields |> List.forall (hasRequiredField pairs)

System.IO.File.ReadAllText("day-04-input.txt")
  |> fun x -> x.Split "\n\n" // isolate the password records
  |> Seq.map (fun x -> Regex.Matches(x, @"([\S+]+):([\S+]+)")) // break them into name:value pairs
  |> Seq.filter isValidPassport
  |> Seq.length
  |> printfn "%d"
