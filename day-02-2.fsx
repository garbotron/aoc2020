open System.Text.RegularExpressions

type Record = { Pos1: int; Pos2: int; Char: char; Password: string }

let isValidPassword record =
  let charAtPos1 = record.Password.[record.Pos1 - 1]
  let charAtPos2 = record.Password.[record.Pos2 - 1]
  let matches = (if charAtPos1 = record.Char then 1 else 0) + (if charAtPos2 = record.Char then 1 else 0)
  matches = 1

System.IO.File.ReadLines("day-02-input.txt")
  |> Seq.map (fun x -> Regex.Match(x, "([0-9]+)-([0-9]+) ([a-z]): (.*)"))
  |> Seq.filter (fun x -> x.Success)
  |> Seq.map (fun x -> { Pos1 = int x.Groups.[1].Value
                         Pos2 = int x.Groups.[2].Value
                         Char = char x.Groups.[3].Value
                         Password = x.Groups.[4].Value })
  |> Seq.filter isValidPassword
  |> Seq.length
  |> printfn "%d"
