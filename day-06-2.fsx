let allYesCount (group: seq<string>) =
  let allYes letter = group |> Seq.forall (Seq.contains letter)
  ['a' .. 'z'] |> Seq.sumBy (fun x -> if allYes x then 1 else 0)

System.IO.File.ReadAllText("day-06-input.txt")
  |> fun x -> x.Split "\n\n" // isolate the groups
  |> Seq.sumBy ((fun x -> x.Trim().Split("\n")) >> allYesCount) // count the number of "all yes" questions per group
  |> printfn "%d"
