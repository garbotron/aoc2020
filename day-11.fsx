type Cell =
  Floor | Empty | Full

  static member Parse (ch: char) =
    match ch with
    | '#' -> Some Full
    | 'L' -> Some Empty
    | '.' -> Some Floor
    | _ -> None

type Grid =
  { Width: int; Cells: Cell[] }

  static member Parse (lines: string[]) =
    { Width = lines.[0].Length
      Cells = lines |> Seq.collect id |> Seq.choose Cell.Parse |> Seq.toArray }

  member this.Height = this.Cells.Length / this.Width

  member this.Cell x y =
    if x < 0 || y < 0 || x >= this.Width || y >= this.Height then None
    else Some this.Cells.[(y * this.Width) + x]

  member this.Neighbors x y =
    [ this.Cell (x - 1) (y - 1)
      this.Cell (x - 1) y
      this.Cell (x - 1) (y + 1)
      this.Cell x (y - 1)
      this.Cell x (y + 1)
      this.Cell (x + 1) (y - 1)
      this.Cell (x + 1) y
      this.Cell (x + 1) (y + 1) ] |> Seq.choose id

  member this.SightedFrom x y =
    [ this.FirstSeatAlongVector x y -1 -1
      this.FirstSeatAlongVector x y -1 0
      this.FirstSeatAlongVector x y -1 1
      this.FirstSeatAlongVector x y 0 -1
      this.FirstSeatAlongVector x y 0 1
      this.FirstSeatAlongVector x y 1 -1
      this.FirstSeatAlongVector x y 1 0
      this.FirstSeatAlongVector x y 1 1 ] |> Seq.choose id

  member this.FirstSeatAlongVector x y dx dy: Cell option =
    match (this.Cell (x + dx) (y + dy)) with
    | None -> None
    | Some Full -> Some Full
    | Some Empty -> Some Empty
    | Some Floor -> this.FirstSeatAlongVector (x + dx) (y + dy) dx dy

  member this.Iterate (nextCell: int -> int -> Cell) =
    let nextCells = seq {
      for y in {0..this.Height-1} do
        for x in {0..this.Width-1} do
          yield nextCell x y
    }
    { Width = this.Width; Cells = Seq.toArray nextCells }

let input = System.IO.File.ReadAllLines("day-11-input.txt") |> Grid.Parse

// Part 1
let rec stabilizePart1 (grid: Grid) =
  let nextCell x y =
    let cell = Option.get (grid.Cell x y)
    let neighbors = grid.Neighbors x y
    if cell = Empty && not (Seq.contains Full neighbors) then
      Full
    elif cell = Full && (neighbors |> Seq.filter ((=) Full) |> Seq.length) >= 4 then
      Empty
    else
      cell
  let next = grid.Iterate nextCell
  if grid = next then next else stabilizePart1 next

input |> stabilizePart1
      |> fun x -> (x.Cells |> Seq.filter ((=) Full) |> Seq.length)
      |> printfn "%d"

// Part 2
let rec stabilizePart2 (grid: Grid) =
  let nextCell x y =
    let cell = Option.get (grid.Cell x y)
    let sighted = grid.SightedFrom x y
    if cell = Empty && not (Seq.contains Full sighted) then
      Full
    elif cell = Full && (sighted |> Seq.filter ((=) Full) |> Seq.length) >= 5 then
      Empty
    else
      cell
  let next = grid.Iterate nextCell
  if grid = next then next else stabilizePart2 next

input |> stabilizePart2
      |> fun x -> (x.Cells |> Seq.filter ((=) Full) |> Seq.length)
      |> printfn "%d"
