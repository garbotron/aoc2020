open System.Text.RegularExpressions

type Record = { Min: int; Max: int; Char: char; Password: string }

let isValidPassword record =
  let count = record.Password |> Seq.filter ((=) record.Char) |> Seq.length
  count >= record.Min && count <= record.Max

System.IO.File.ReadLines("day-02-input.txt")
  |> Seq.map (fun x -> Regex.Match(x, "([0-9]+)-([0-9]+) ([a-z]): (.*)"))
  |> Seq.filter (fun x -> x.Success)
  |> Seq.map (fun x -> { Min = int x.Groups.[1].Value
                         Max = int x.Groups.[2].Value
                         Char = char x.Groups.[3].Value
                         Password = x.Groups.[4].Value })
  |> Seq.filter isValidPassword
  |> Seq.length
  |> printfn "%d"
