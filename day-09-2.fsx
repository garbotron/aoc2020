let getAllRanges (numbers: int64[]): seq<int64[]> =
  seq {
    for i in [0..numbers.Length-2] do
      for j in [i+1..numbers.Length-1] do
        numbers.[i..j]
  }

System.IO.File.ReadAllLines("day-09-input.txt")
  |> Array.map int64
  |> getAllRanges
  |> Seq.find (Seq.sum >> (=) 31161678L)
  |> fun x -> (Seq.min x) + (Seq.max x)
  |> printfn "%d"
