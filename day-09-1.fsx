let allPairs (arr: int[]): seq<int * int> =
  seq {
    for i in [0..arr.Length-2] do
      for j in [i+1..arr.Length-1] do
        (arr.[i], arr.[j])
  }

let isNumberValid preambleLen (window: int[]): bool =
  let preamble = window.[0..preambleLen-1]
  let num = window.[preambleLen]
  allPairs preamble |> Seq.exists (fun (x, y) -> x + y = num)

System.IO.File.ReadAllLines("day-09-input.txt")
  |> Seq.map int
  |> Seq.windowed 26
  |> Seq.find (isNumberValid 25 >> not)
  |> Seq.last
  |> printfn "%d"
