let input = System.IO.File.ReadAllLines("day-13-input.txt")
let minTs = int input.[0]
let optBusIds = input.[1].Split ',' |> Seq.map (fun x -> if x = "x" then None else Some (int x))
let busIds = optBusIds |> Seq.choose id

// Part 1
let findBusThatLeavesAt (ts: int) = busIds |> Seq.tryFind ((fun x -> ts % x = 0))
Seq.initInfinite (fun x -> minTs + x)
  |> Seq.choose findBusThatLeavesAt
  |> Seq.head
  |> fun x -> (x - (minTs % x)) * x
  |> printfn "%d"

// Part 2

// Break the problem down into a series of congruences, then apply the Chinese Remainder Theorem (thanks to a friend
// for pointing me in this direction).

// Using the example input, we know { t % 7 = 0,  (t + 1) % 13 = 0,   (t + 4) % 59 = 0,   ... }
// This means that, equivalently,   { t % 7 = 0,  t % 13       = 12,  t % 59       = 55,  ... }

// I cheat and use a F# implementation of the Chinese Remainder Theorem taken from Rosetta Code
// (https://rosettacode.org/wiki/Chinese_remainder_theorem#sieving).

let rec sieve cs x m = // not my code
  match cs with
  | [] -> Some(x)
  | (a,n)::rest ->
    let arrProgress = Seq.unfold (fun x -> Some(x, x + m)) x
    let firstXmodNequalA = Seq.tryFind (fun x -> a = x % n)
    match firstXmodNequalA (Seq.take (int n) arrProgress) with
    | None -> None
    | Some(x) -> sieve rest x (m * n)

let crt congruences = // not my code
  let cs =
    congruences
    |> List.map (fun (a,n) -> (a % n, n))
    |> List.sortBy (snd>>(~-))
  let an = List.head cs
  match sieve (List.tail cs) (fst an) (snd an) with
  | None    -> printfn "no solution"
  | Some(x) -> printfn "%d" x

let modulo n m = ((n % m) + m) % m
optBusIds
  |> Seq.indexed
  |> Seq.choose (fun (i, x) -> match x with
                               | Some n -> Some (int64 i, int64 n)
                               | None -> None)
  |> Seq.map (fun (i, n) -> (modulo (n - i) n, n))
  |> Seq.toList
  |> crt
