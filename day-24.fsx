type Dir = E | W | NE | NW | SE | SW
let AllDirs = [E; W; NE; NW; SE; SW]

let rec parseLine (str: string): Dir list =
  AllDirs
  |> List.map (fun x -> x.ToString().ToLower(), x)
  |> List.tryFind (fun (x, _) -> str.StartsWith x)
  |> function
     | None -> []
     | Some (x, y) -> [y] @ (parseLine str.[x.Length..])

let step (x, y) = function
  | E -> x - 1, y
  | W -> x + 1, y
  | NE -> x - 1, y - 1
  | NW -> x, y - 1
  | SE -> x, y + 1
  | SW -> x + 1, y + 1

let neighbors xy = AllDirs |> List.map (step xy)
let selfAndNeighbors xy = xy :: neighbors xy
let regionAround xys = xys |> Set.toSeq |> Seq.collect selfAndNeighbors |> Set

// Is the selected tile black at the end of the day?
let isBlack blackTiles xy =
  let blackNeighbors = xy |> neighbors |> List.filter (fun x -> blackTiles |> Set.contains x)
  match blackTiles |> Set.contains xy, blackNeighbors.Length with
  | true, x when x = 0 || x > 2 -> false
  | true, _ -> true
  | false, 2 -> true
  | false, _ -> false

let passDay blackTiles = blackTiles |> regionAround |> Set.filter (isBlack blackTiles)
let passDays count blackTiles = { 1 .. count } |> Seq.fold (fun x _ -> passDay x) blackTiles

let blackTiles =
  System.IO.File.ReadAllLines "day-24-input.txt"
  |> Array.toList
  |> List.map (parseLine >> List.fold step (0, 0))
  |> List.groupBy id
  |> List.filter (fun (_, x) -> x.Length % 2 = 1)
  |> List.map fst
  |> Set

// Part 1
printfn "%d" (blackTiles |> Set.count)

// Part 2
printfn "%d" (blackTiles |> passDays 100 |> Set.count)
