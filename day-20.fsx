type Tile =
  { Id: int; Size: int; Data: Set<int * int> }
  member this.Edge coords =
    coords |> Seq.indexed |> Seq.filter (fun (_, x) -> Set.contains x this.Data) |> Seq.sumBy (fun (i, _) -> 1 <<< i)
  member this.Transformed transform = { this with Data = this.Data |> Set.map transform }
  member this.L = this.Edge ({ 0 .. this.Size - 1 } |> Seq.map (fun x -> 0, x))
  member this.R = this.Edge ({ 0 .. this.Size - 1 } |> Seq.map (fun x -> this.Size - 1, x))
  member this.T = this.Edge ({ 0 .. this.Size - 1 } |> Seq.map (fun y -> y, 0))
  member this.B = this.Edge ({ 0 .. this.Size - 1 } |> Seq.map (fun y -> y, this.Size - 1))
  member this.FlipHorizontal = this.Transformed (fun (x, y) -> (this.Size - 1) - x, y)
  member this.FlipVertical = this.Transformed (fun (x, y) -> x, (this.Size - 1 ) - y)
  member this.Rotate90 = this.Transformed (fun (x, y) -> (this.Size - 1) - y, x)
  member this.Rotate180 = this.Transformed (fun (x, y) -> (this.Size - 1) - x, (this.Size - 1) - y)
  member this.Rotate270 = this.Transformed (fun (x, y) -> y, (this.Size - 1) - x)
  member this.AllFlips = [this; this.FlipHorizontal; this.FlipVertical; this.FlipHorizontal.FlipVertical]
  member this.AllRotations = [this; this.Rotate90; this.Rotate180; this.Rotate270]
  member this.AllTransforms = this.AllRotations |> List.collect (fun x -> x.AllFlips)
  member this.WithoutBorders =
    { this with
        Size = this.Size - 2
        Data = this.Data
          |> Set.toSeq
          |> Seq.filter (fun (x, y) -> x > 0 && y > 0 && x < this.Size - 1 && y < this.Size - 1)
          |> Seq.map (fun (x, y) -> x - 1, y - 1)
          |> Set }

let parseTileData lines: Set<int * int> =
  let mapLine (y, s) = s |> Seq.indexed |> Seq.filter (fun (_, c) -> c = '#') |> Seq.map (fun (x, _) -> x, y)
  lines |> Seq.indexed |> Seq.collect mapLine |> Set

let parseTile (str: string): Tile =
  let lines = str.Split "\n"
  { Id = int lines.[0].[5..lines.[0].Length-2]
    Size = lines.[1].Length
    Data = lines.[1..] |> parseTileData }

let input =
  System.IO.File.ReadAllText("day-20-input.txt")
  |> fun x -> x.Split "\n\n"
  |> Array.map ((fun x -> x.Trim()) >> parseTile)
let tileCount = input.Length
let squareLen = int (System.Math.Sqrt (float tileCount))

let allTiles = input |> Seq.collect (fun x -> x.AllTransforms) |> Seq.toList

let getPossibleNextTiles (tiles: Tile list) (unused: Set<Tile>): Tile seq =
  let matchesTop (tile: Tile) =
     if tiles.Length < squareLen then true
     else tiles.[tiles.Length - squareLen].B = tile.T
  let matchesLeft (tile: Tile) =
     if tiles.Length % squareLen = 0 then true
     else tiles.[tiles.Length - 1].R = tile.L
  unused |> Set.filter (fun x -> matchesTop x && matchesLeft x) |> Set.toSeq

let rec solve (tiles: Tile list) (unused: Set<Tile>): seq<Tile list * Set<Tile>> =
  if Set.isEmpty unused then seq { tiles, unused }
  else
    let nextTiles = getPossibleNextTiles tiles unused
    nextTiles |> Seq.collect (fun t -> solve (tiles @ [t]) (unused |> Set.filter (fun x -> x.Id <> t.Id)))

// Part 1
let solution = solve [] (Set allTiles) |> Seq.head |> fst
solution
  |> List.map (fun x -> int64 x.Id)
  |> fun x -> x.[0] * x.[squareLen - 1] * x.[x.Length - squareLen] * x.[x.Length - 1]
  |> printfn "%d"

// Part 2
let allCoords len = seq { for y in { 0 .. len - 1 } do for x in { 0 .. len - 1 } do yield x, y }
let borderless = solution |> List.map (fun t -> t.WithoutBorders)
let tileSize = borderless.[0].Size
let combinedImage =
  { Id = 0
    Size = squareLen * tileSize
    Data = allCoords squareLen
      |> Seq.collect (fun (x, y) ->
          let i = (y * squareLen) + x
          borderless.[i].Data |> Set.toSeq |> Seq.map (fun (a, b) -> a + (x * tileSize), b + (y * tileSize)))
      |> Set }

let seaMonster =
  [ "                  # "
    "#    ##    ##    ###"
    " #  #  #  #  #  #   " ] |> parseTileData

let seaMonstersInImage img =
  allCoords img.Size
  |> Seq.map (fun (x, y) -> seaMonster |> Set.map (fun (a, b) -> a + x, b + y))
  |> Seq.filter (Set.isSuperset img.Data)
  |> Seq.length

let seaMonsterCount = combinedImage.AllTransforms |> Seq.map seaMonstersInImage |> Seq.max
printfn "%d" ((Set.count combinedImage.Data) - (seaMonsterCount * (Set.count seaMonster)))
