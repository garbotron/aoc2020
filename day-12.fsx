type Dir = North | East | South | West | Left | Right | Forward
type Instruction = { Dir: Dir; By: int }
type StateP1 = { X: int; Y: int; Facing: int }
type StateP2 = { X: int; Y: int; WX: int; WY: int } // (WX, WY) = waypoint coords, relative to ship

let parseInstruction (str: string) =
  { Dir = match str.[0] with
          | 'N' -> North
          | 'E' -> East
          | 'S' -> South
          | 'W' -> West
          | 'L' -> Left
          | 'R' -> Right
          | 'F' -> Forward
          | _ -> raise <| System.Exception()
    By = int str.[1..] }

let rotate (x: int) (r: int) =
  let ret = x + r
  if ret < 0 then ret + 360
  elif ret >= 360 then ret - 360
  else ret

let rec execP1 (state: StateP1) (instr: Instruction): StateP1 =
  match instr.Dir with
  | North -> { state with Y = state.Y - instr.By }
  | East -> { state with X = state.X + instr.By }
  | South -> { state with Y = state.Y + instr.By }
  | West -> { state with X = state.X - instr.By }
  | Left -> { state with Facing = rotate state.Facing -instr.By }
  | Right -> { state with Facing = rotate state.Facing instr.By }
  | Forward when state.Facing = 0 -> execP1 state { instr with Dir = East }
  | Forward when state.Facing = 90 -> execP1 state { instr with Dir = South }
  | Forward when state.Facing = 180 -> execP1 state { instr with Dir = West }
  | Forward when state.Facing = 270 -> execP1 state { instr with Dir = North }
  | _ -> raise <| System.Exception()

let rec execP2 (state: StateP2) (instr: Instruction): StateP2 =
  match instr.Dir with
  | North -> { state with WY = state.WY + instr.By }
  | East -> { state with WX = state.WX + instr.By }
  | South -> { state with WY = state.WY - instr.By }
  | West -> { state with WX = state.WX - instr.By }
  | Left when instr.By = 90 -> { state with WX = -state.WY; WY = state.WX }
  | Right when instr.By = 90 -> { state with WX = state.WY; WY = -state.WX }
  | Left | Right -> execP2 (execP2 state { instr with By = 90 }) { instr with By = instr.By - 90 }
  | Forward -> { state with X = state.X + (state.WX * instr.By); Y = state.Y + (state.WY * instr.By) }
  | _ -> raise <| System.Exception()

// Part 1
System.IO.File.ReadAllLines("day-12-input.txt")
  |> Seq.map parseInstruction
  |> Seq.fold execP1 { X = 0; Y = 0; Facing = 0 }
  |> fun x -> printfn "%d" ((System.Math.Abs x.X) + (System.Math.Abs x.Y))

// Part 2
System.IO.File.ReadAllLines("day-12-input.txt")
  |> Seq.map parseInstruction
  |> Seq.fold execP2 { X = 0; Y = 0; WX = 10; WY = 1; }
  |> fun x -> printfn "%d" ((System.Math.Abs x.X) + (System.Math.Abs x.Y))
