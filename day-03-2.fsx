let inputLines = System.IO.File.ReadLines("day-03-input.txt") |> Array.ofSeq

let isOnMap y = y < inputLines.Length && inputLines.[y] <> ""
let isTree x y =
  let line = inputLines.[y]
  let ch = line.[x % line.Length]
  ch = '#'

let rec hop x y xStep yStep treeCount =
  if isOnMap y then
    let tree = if isTree x y then 1 else 0
    hop (x + xStep) (y + yStep) xStep yStep (treeCount + tree)
  else treeCount

let slopes = [
  hop 0 0 1 1 0
  hop 0 0 3 1 0
  hop 0 0 5 1 0
  hop 0 0 7 1 0
  hop 0 0 1 2 0
]

printfn "%d" (slopes |> Seq.map int64 |> Seq.fold (*) 1L)
