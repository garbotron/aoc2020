type Instruction = Nop of int | Acc of int | Jmp of int

let parseInstruction (line: string): Instruction =
  match line.[0..2] with
  | "acc" -> Acc (int line.[4..])
  | "jmp" -> Jmp (int line.[4..])
  | _ -> Nop (int line.[4..])

// Replace one element in an array, leaving the rest intact.
let replaceElem idx elem = Array.mapi (fun i x -> if i = idx then elem else x)

// Execute a program from the given line. Return Some <accumulator> if program finishes, None if it hangs.
let rec execute (line: int) (program: Instruction[]) (executed: bool[]) (accum: int): int option =
  if line >= program.Length then
    Some accum // successful finish
  elif executed.[line] then
    None // second execution of this instruction (hang)
  else
    let executed = replaceElem line true executed
    match program.[line] with
    | Acc x -> execute (line + 1) program executed (accum + x)
    | Jmp x -> execute (line + x) program executed accum
    | Nop _ -> execute (line + 1) program executed accum

// Perform the above, but with one of the lines corrected (jmp->nop or nop->jmp).
let executeWithCorrection (line: int) (program: Instruction[]): int option =
  let executed = Array.create program.Length false
  match program.[line] with
  | Jmp x -> execute 0 (replaceElem line (Nop x) program) executed 0
  | Nop x -> execute 0 (replaceElem line (Jmp x) program) executed 0
  | _ -> None // line can't be corrected

// Try every possible line correction and output the accumulator once we find one that finishes.
let program = System.IO.File.ReadAllLines("day-08-input.txt") |> Array.map parseInstruction
[0..(program.Length - 1)]
  |> Seq.map (fun i -> executeWithCorrection i program)
  |> Seq.choose id
  |> Seq.head
  |> printfn "%d"
