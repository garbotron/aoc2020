type Instruction = Nop | Acc of int | Jmp of int

let parseInstruction (line: string): Instruction =
  match line.[0..2] with
  | "acc" -> Acc (int line.[4..])
  | "jmp" -> Jmp (int line.[4..])
  | _ -> Nop

// Replace one element in an array, leaving the rest intact.
let replaceElem idx elem = Array.mapi (fun i x -> if i = idx then elem else x)

// Execute a line of the program and keep running until we execute an instruction twice, returning the accumulator.
let rec execute (line: int) (program: Instruction[]) (executed: bool[]) (accum: int): int =
  if executed.[line] then
    accum // second execution of this instruction
  else
    let executed = replaceElem line true executed
    match program.[line] with
    | Acc x -> execute (line + 1) program executed (accum + x)
    | Jmp x -> execute (line + x) program executed accum
    | Nop -> execute (line + 1) program executed accum

let program = System.IO.File.ReadAllLines("day-08-input.txt") |> Array.map parseInstruction
let executed = Array.create program.Length false
printfn "%d" (execute 0 program executed 0)
