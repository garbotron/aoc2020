open System.Text.RegularExpressions

type Constraint = { Allergen: string; MustBeOneOf: Set<string> }
type State = {
  Allergens: Set<string>
  Ingredients: Set<string>
  AllIngedients: string list // used to calculate ttotal number of occurences
  Constraints: Constraint list }

let parseInputLine (input: State) (line: string): State =
  let m = Regex.Match (line, @"(?:(\w+) )+\(contains (?:(\w+)(?:, )?)+")
  let vars = m.Groups.[1].Captures |> Seq.toList |> List.map (fun x -> x.Value)
  let contains = m.Groups.[2].Captures |> Seq.toList |> List.map (fun x -> x.Value)
  { Allergens = contains |> Seq.fold (fun set s -> Set.add s set) input.Allergens
    Ingredients = vars |> Seq.fold (fun set s -> Set.add s set) input.Ingredients
    AllIngedients = input.AllIngedients @ vars
    Constraints = input.Constraints @
                  (contains |> List.map (fun x -> { Allergen = x; MustBeOneOf = vars |> Set.ofList })) }

let input =
  System.IO.File.ReadAllLines "day-21-input.txt"
  |> Seq.fold parseInputLine { Allergens = Set.empty; Ingredients = Set.empty; AllIngedients = []; Constraints = [] }

// For all allergens with multiple constraints, flatten them using set intersection.
let flattenConstraints (state: State): State =
  let folder (state: Set<string>) (elem: Constraint): Set<string> =
    if Set.isEmpty state then elem.MustBeOneOf else Set.intersect state elem.MustBeOneOf
  { state with
      Constraints = state.Constraints
      |> List.groupBy (fun x -> x.Allergen)
      |> List.map (fun (x, y) -> { Allergen = x; MustBeOneOf = y |> List.fold folder Set.empty }) }

// Remove any solved parts of the constraints for all given allergens.
// For example, if we know "dairy" must be "A" or "B", and "soy" must be "A", "B" or "C", we know soy is "C".
// Recurses until no new match is found.
let rec removeSolvedAllergens (state: State): State =
  let scanForSolved (allergen: string): State option =
    let myConstraint = state.Constraints |> Seq.find (fun x -> x.Allergen = allergen)
    let otherConstraints = state.Constraints |> List.except [myConstraint]
    match otherConstraints |> List.tryFind (fun x -> Set.isSubset myConstraint.MustBeOneOf x.MustBeOneOf) with
    | None -> None
    | Some x ->
      let newSet = Set.difference x.MustBeOneOf myConstraint.MustBeOneOf
      Some { state with Constraints = { x with MustBeOneOf = newSet } :: state.Constraints |> List.except [x] }
  match state.Allergens |> Set.toSeq |> Seq.choose scanForSolved |> Seq.tryHead with
  | None -> state
  | Some x -> removeSolvedAllergens x

// Part 1
let result = input |> flattenConstraints |> removeSolvedAllergens
let couldHaveAllergen = result.Constraints |> Seq.collect (fun x -> x.MustBeOneOf |> Set.toSeq) |> Set
printfn "%d" (result.AllIngedients |> Seq.filter (fun x -> not (Set.contains x couldHaveAllergen)) |> Seq.length)

// Part 2
let findAllergen s = result.Constraints |> Seq.find (fun x -> Set.contains s x.MustBeOneOf) |> fun x -> x.Allergen
printfn "%s" (System.String.Join (",", couldHaveAllergen |> Set.toSeq |> Seq.sortBy findAllergen))
