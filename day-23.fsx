let input = "135468729" |> Seq.map (string >> int)
let extendedInput = seq { yield! input; yield! { (Seq.max input) + 1 .. 1000000 } }

type Cup = { Id: int; mutable Next: Cup option }
type Circle = { Cur: Cup; Map: Map<int, Cup>; LowestId: int; HighestId: int }

let initCircle numbers =
  numbers
  |> Seq.toList
  |> List.map (fun x -> { Id = x; Next = None })
  |> fun x -> x @ [List.head x]
  |> List.pairwise
  |> List.map (fun (a, b) -> a.Next <- Some b; a)
  |> fun x -> { Cur = List.head x
                Map = x |> List.map (fun x -> x.Id, x) |> Map
                LowestId = x |> List.map (fun x -> x.Id) |> List.min
                HighestId = x |> List.map (fun x -> x.Id) |> List.max }

let rec next count cup =
  let n = Option.get cup.Next
  if count = 1 then [n] else n :: next (count - 1) n

let rec find id cup =
  if cup.Id = id then cup else find id (Option.get cup.Next)

let move circle: Circle =
  let next3 = circle.Cur |> next 3
  let rec findDest = function
    | x when x < circle.LowestId -> findDest circle.HighestId
    | x when next3 |> List.exists (fun y -> y.Id = x) -> findDest (x - 1)
    | x when Map.containsKey x circle.Map -> circle.Map.[x]
    | x -> findDest (x - 1)
  let rec dest = findDest (circle.Cur.Id - 1)
  let afterDest = dest.Next
  dest.Next <- circle.Cur.Next
  circle.Cur.Next <- next3.[2].Next
  next3.[2].Next <- afterDest
  { circle with Cur = Option.get circle.Cur.Next }

// Part 1
{ 1 .. 100 }
  |> Seq.fold (fun x _ -> move x) (initCircle input)
  |> fun x -> x.Cur |> next (Seq.length input - 1)
  |> List.map (fun x -> string x.Id)
  |> List.fold (+) ""
  |> printfn "%s"

// Part 2
{ 1 .. 10000000 }
  |> Seq.fold (fun x _ -> move x) (initCircle extendedInput)
  |> fun x -> x.Cur |> find 1 |> next 2
  |> List.map (fun x -> int64 x.Id)
  |> List.fold (*) 1L
  |> printfn "%d"
