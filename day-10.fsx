let input = System.IO.File.ReadAllLines("day-10-input.txt")
          |> Array.map int64
          |> Array.sort

// Part 1: sort and scan the input sequence pairwise.
input |> fun x -> seq { yield 0L; yield! x; yield (Seq.last x + 3L) }
      |> Seq.pairwise
      |> Seq.map (fun (x, y) -> y - x)
      |> fun diffs ->
           let num1s = diffs |> Seq.filter ((=) 1L) |> Seq.length
           let num3s = diffs |> Seq.filter ((=) 3L) |> Seq.length
           printfn "%d" (num1s * num3s)

// Part 2: dynamic programming: fill a parallel cache array with the number of paths to reach each index.
let cache = Array.create input.Length 0L
for i in seq { 0..input.Length-1 } do
  let num = input.[i]
  let pathsFromRoot = if num <= 3L then 1L else 0L
  let pathsFromSibling offset =
    let idx = i + offset
    if idx < 0 then 0L
    elif num - input.[idx] > 3L then 0L
    else cache.[idx]
  let pathsFromSiblings = seq { -3..-1 } |> ((Seq.map pathsFromSibling) >> Seq.sum)
  cache.[i] <- pathsFromRoot + pathsFromSiblings

printfn "%d" (cache.[input.Length - 1])
