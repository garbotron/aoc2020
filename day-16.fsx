open System.Text.RegularExpressions

type Rule = { Name: string; Ranges: (int * int) list }

let input = System.IO.File.ReadAllText("day-16-input.txt")

let rules =
  Regex.Matches (input, "([a-z ]+): ([0-9]+)-([0-9]+) or ([0-9]+)-([0-9]+)")
  |> Seq.map (fun m -> {
    Name = m.Groups.[1].Value
    Ranges = [
      (int m.Groups.[2].Value, int m.Groups.[3].Value)
      (int m.Groups.[4].Value, int m.Groups.[5].Value)
    ]})

let myTicket =
  Regex.Match (input, @"your ticket:\s+([0-9,]+)")
  |> fun m -> m.Groups.[1].Value.Split ','
  |> Array.map int

let nearbyTickets =
  Regex.Match (input, @"nearby tickets:(?:\s+([0-9,]+))+")
  |> fun m -> m.Groups.[1].Captures
  |> Seq.map (fun c -> (c.Value.Split ',') |> Array.map int)

let isValidForRule v rule = rule.Ranges |> Seq.exists (fun (min, max) -> v >= min && v <= max)
let isValidForRules v = rules |> Seq.exists (isValidForRule v)

// Part 1
nearbyTickets
  |> Seq.collect id
  |> Seq.filter (not << isValidForRules)
  |> Seq.sum
  |> printfn "%d"

// Part 2
let validNearbyTickets = nearbyTickets |> Seq.filter (Seq.forall isValidForRules) |> Seq.toList

type State = {
  UnmappedRules: Set<Rule>
  ColumnToRule: Map<int, Rule>
}

let rec findColumns (s: State): State =
  if Set.isEmpty s.UnmappedRules then s
  else
    let isColumnValidForRule rule col =
      if Option.isSome (s.ColumnToRule |> Map.tryFind col) then false
      else validNearbyTickets |> Seq.forall (fun t -> isValidForRule t.[col] rule)
    let validColumnsForRule rule = { 0 .. myTicket.Length - 1 } |> Seq.filter (isColumnValidForRule rule)
    let rule, col =
      s.UnmappedRules
      |> Set.toSeq
      |> Seq.map (fun r -> (r, validColumnsForRule r))
      |> Seq.choose (fun (r, c) -> match (Seq.tryExactlyOne c) with | None -> None | Some x -> Some (r, x))
      |> Seq.head
    findColumns {
      UnmappedRules = s.UnmappedRules |> Set.remove rule
      ColumnToRule = s.ColumnToRule |> Map.add col rule }

findColumns { UnmappedRules = Set.ofSeq rules; ColumnToRule = Map.empty }
  |> fun s -> s.ColumnToRule |> Map.toSeq
  |> Seq.filter (fun (_, r) -> r.Name.StartsWith "departure")
  |> Seq.map (fun (i, _) -> myTicket.[i])
  |> Seq.fold (fun x y -> x * (int64 y)) 1L
  |> printfn "%d"
