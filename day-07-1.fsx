open System.Collections.Generic
open System.Text.RegularExpressions

type Bag = { Color: string; Holds: seq<int * string> }

// Parse a bag and what it holds from an input line.
let parseBag (str: string): Bag =
  let regexMatch = Regex.Match(str, "(.*) bags contain (([0-9]+) ([^.,]*) bags?[, ]*)*")
  let holdsCounts = regexMatch.Groups.[3].Captures |> Seq.map (fun x -> int x.Value)
  let holdsColors = regexMatch.Groups.[4].Captures |> Seq.map (fun x -> x.Value)
  { Color = regexMatch.Groups.[1].Value; Holds = Seq.zip holdsCounts holdsColors }

// Create a mapping of [color] -> [bag record].
let bags = System.IO.File.ReadAllLines("day-07-input.txt")
         |> Seq.map(parseBag >> fun x -> (x.Color, x))
         |> dict

// Check if a given bag can contain another given bag (recursively).
let rec bagCanContain (inner: string) (outer: string): bool =
  bags.[outer].Holds |> Seq.exists (fun (c, b) -> inner = b || bagCanContain inner b)

// Count the number of bags that (ultimately) can contain shiny gold.
bags.Keys |> Seq.filter (bagCanContain "shiny gold")
          |> Seq.length
          |> printfn "%d"
