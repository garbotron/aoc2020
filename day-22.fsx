let deck1, deck2 =
  let input = System.IO.File.ReadAllText "day-22-input.txt"
  let parseDeck (str: string) = str.Split "\n" |> Seq.skip 1 |> Seq.map int |> Seq.toList
  input.Trim().Split "\n\n" |> fun x -> parseDeck x.[0], parseDeck x.[1]

let score deck = deck |> Seq.rev |> Seq.indexed |> Seq.sumBy (fun (i, x) -> (i + 1) * x)

// Part 1
let rec combat = function
  | h1 :: t1, h2 :: t2 -> combat <| if h1 > h2 then (t1 @ [h1; h2], t2) else (t1, t2 @ [h2; h1])
  | x -> x // one of the decks is empty, stop recursion

printfn "%d" (combat (deck1, deck2) |> fun (x, y) -> score x + score y)

// Part 2
let rec rCombat deck1 deck2 cache =
  if cache |> Set.contains (deck1, deck2) then deck1, [], cache // player 1 win due to infinite recursion
  else
    let cache = cache |> Set.add (deck1, deck2)
    let subGame d1 d2 = match rCombat d1 d2 Set.empty with | _, [], _ -> 1  | _ -> 2
    match deck1, deck2 with
    | h1 :: t1, h2 :: t2 when t1.Length >= h1 && t2.Length >= h2 -> // play sub-game to determine next winner
      match subGame (List.take h1 t1) (List.take h2 t2) with
      | 1 -> rCombat (t1 @ [h1; h2]) t2 cache // P1 sub-game win
      | _ -> rCombat t1 (t2 @ [h2; h1]) cache // P2 sub-game win
    | h1 :: t1, h2 :: t2 when h1 > h2 -> rCombat (t1 @ [h1; h2]) t2 cache // P1 normal win
    | h1 :: t1, h2 :: t2 -> rCombat t1 (t2 @ [h2; h1]) cache // P2 normal win
    | d1, d2 -> d1, d2, cache // one of the decks is empty, stop recursion

printfn "%d" (rCombat deck1 deck2 Set.empty |> fun (x, y, _) -> score x + score y)
