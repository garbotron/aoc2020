let decodeNumber (zero: char) (one: char) (str: string) =
  let binaryStr = str.Replace(zero, '0').Replace(one, '1')
  System.Convert.ToInt32(binaryStr, 2)

let getSeatId (boardingPass: string) =
  let row = decodeNumber 'F' 'B' boardingPass.[0 .. 6]
  let col = decodeNumber 'L' 'R' boardingPass.[7 .. 9]
  (row * 8) + col

System.IO.File.ReadAllLines("day-05-input.txt")
  |> Seq.map getSeatId // convert all boarding passes to seat IDs
  |> Seq.sort // sort the seat IDs numerically
  |> Seq.windowed 2 // take each pair of seat IDs
  |> Seq.find (fun x -> x.[1] - x.[0] = 2) // find the pair that is 2 apart (the missing seat)
  |> fun x -> printfn "%d" (x.[0] + 1) // print the missing seat ID
